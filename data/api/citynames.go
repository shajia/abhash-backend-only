package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Pos struct {
	Lon float64
	Lat float64
}
type City struct {
	ID       int
	Name     string
	Country  string
	Position Pos
}

var CityByID map[string]int

func init() {

	dat, err := ioutil.ReadFile("city.list.json")
	fmt.Println(err)
	var cities []City
	json.Unmarshal([]byte(dat), &cities)

	CityByID = make(map[string]int)

	for _, val := range cities {
		CityByID[val.Name] = val.ID

	}
}
