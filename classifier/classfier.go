package classifier

import (
	"strings"
)

//NewQuestion Ask question for classification
func NewQuestion(input string) int {
	if type1(input) {
		return 1
	}

	if type2(input) {
		return 2
	}

	if type3(input) {
		return 3
	}

	if type4(input) {
		return 4
	}

	if type5(input) {
		return 5
	}

	return -1
}

func type2(input string) bool {
	words := strings.Split(input, " ")

	var isRain bool
	var isToday bool

	for _, e := range words {
		//fmt.Println(e)
		if e == "rain" || e == "Rain" || e == "Raining" || e == "shower" || e == "raining?" {
			isRain = true
		}

		if e == "today" || e == "today?" {
			isToday = true
		}
	}

	if isRain && isToday {
		return true
	}
	return false
}

func type1(input string) bool {
	words := strings.Split(input, " ")

	var isWeather bool
	var isNow bool

	for _, e := range words {

		//fmt.Println(e)
		if e == "weather" {
			isWeather = true
		}

		if e == "today" || e == "now" || e == "current" || e == "now?" || e == "today?" {
			isNow = true
		}
	}
	//fmt.Println("Printing")
	//fmt.Println(isWeather)
	//fmt.Println(isNow)
	if isWeather && isNow {
		return true
	}
	return false
}

func type3(input string) bool {
	words := strings.Split(input, " ")

	var isWeather bool
	var isForecast bool
	var isTomorrow bool

	for _, e := range words {
		if e == "weather" {
			isWeather = true
		}

		if e == "tomorrow" {
			isTomorrow = true
		}

		if e == "forecast" {
			isForecast = true
		}
	}

	if isForecast && (isWeather || isTomorrow) {
		return true
	}
	//fmt.Println(isWeather)
	//fmt.Println(isForecast)
	return false

}

func type4(input string) bool {
	words := strings.Split(input, " ")

	var isSunrise bool
	var isTomorrow bool

	for _, e := range words {
		if e == "sunrise" {
			isSunrise = true
		}

		if e == "today" || e == "today?" {
			isTomorrow = true
		}
	}

	if isSunrise && isTomorrow {
		return true
	}

	return false
}

func type5(input string) bool {
	words := strings.Split(input, " ")

	var isSunset bool
	var isToday bool

	for _, e := range words {
		if e == "sunset" {
			isSunset = true
		}

		if e == "today" || e == "today?" {
			isToday = true
		}
	}

	if isSunset && isToday {
		return true
	}

	return false
}
