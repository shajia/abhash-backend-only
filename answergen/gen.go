package answergen

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type WeatherForecast struct {
	Cod     string  `json:"cod"`
	Message float64 `json:"message"`
	Cnt     int     `json:"cnt"`
	List    []struct {
		Dt   int `json:"dt"`
		Main struct {
			Temp      float64 `json:"temp"`
			TempMin   float64 `json:"temp_min"`
			TempMax   float64 `json:"temp_max"`
			Pressure  float64 `json:"pressure"`
			SeaLevel  float64 `json:"sea_level"`
			GrndLevel float64 `json:"grnd_level"`
			Humidity  int     `json:"humidity"`
			TempKf    float64 `json:"temp_kf"`
		} `json:"main"`
		Weather []struct {
			ID          int    `json:"id"`
			Main        string `json:"main"`
			Description string `json:"description"`
			Icon        string `json:"icon"`
		} `json:"weather"`
		Clouds struct {
			All int `json:"all"`
		} `json:"clouds"`
		Wind struct {
			Speed float64 `json:"speed"`
			Deg   float64 `json:"deg"`
		} `json:"wind"`
		Snow struct {
		} `json:"snow"`
		Sys struct {
			Pod string `json:"pod"`
		} `json:"sys"`
		DtTxt string `json:"dt_txt"`
	} `json:"list"`
	City struct {
		ID    int    `json:"id"`
		Name  string `json:"name"`
		Coord struct {
			Lat float64 `json:"lat"`
			Lon float64 `json:"lon"`
		} `json:"coord"`
		Country string `json:"country"`
	} `json:"city"`
}

type CityWeather struct {
	Coord struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	} `json:"coord"`
	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp     float64 `json:"temp"`
		Pressure int     `json:"pressure"`
		Humidity int     `json:"humidity"`
		TempMin  float64 `json:"temp_min"`
		TempMax  float64 `json:"temp_max"`
	} `json:"main"`
	Visibility int `json:"visibility"`
	Wind       struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt  int `json:"dt"`
	Sys struct {
		Type    int     `json:"type"`
		ID      int     `json:"id"`
		Message float64 `json:"message"`
		Country string  `json:"country"`
		Sunrise int     `json:"sunrise"`
		Sunset  int     `json:"sunset"`
	} `json:"sys"`
	ID   int    `json:"id"`
	Name string `json:"name"`
	Cod  int    `json:"cod"`
}

func answerToday(response *http.Response, q_type int) {
	data, _ := ioutil.ReadAll(response.Body)
	//fmt.Println(string(data))
	var Cur CityWeather
	json.Unmarshal([]byte(data), &Cur)
	//fmt.Println(Cur)
	if q_type == 1 {
		fmt.Println("The temparature is", Cur.Main.Temp-273.15, "degree in", Cur.Name, ". Humidity is", Cur.Main.Humidity, "percent. The general weather can be described as", Cur.Weather[0].Main, ".")
	}
	if q_type == 4 {
		fmt.Println("The sunrise in", Cur.Name, "is at", time.Unix(int64(Cur.Sys.Sunrise), 0))
	}

	if q_type == 5 {
		//fmt.Println(Cur.Sys.Sunrise)
		//fmt.Println(Cur.Sys.Sunset)
		fmt.Println("The sunset in", Cur.Name, "is at", time.Unix(int64(Cur.Sys.Sunset), 0))
	}
}

func answerTomorrow(response *http.Response) {
	data, _ := ioutil.ReadAll(response.Body)

	var Cur WeatherForecast
	json.Unmarshal([]byte(data), &Cur)

	fmt.Println("The weather forcast for tomorrow for ", Cur.City.Name, ": Humidity: ", Cur.List[0].Main.Humidity, "Temparature:", Cur.List[0].Main.Temp-273.15)
}

func NewAnswer(apistring string, q_type int) {
	response, err := http.Get(apistring)
	fmt.Println(err)

	if q_type == 1 || q_type == 4 || q_type == 5 {
		answerToday(response, q_type)
	}

	if q_type == 3 {
		answerTomorrow(response)
	}
	//fmt.Println(response)
	//data, _ := ioutil.ReadAll(response.Body)
	//fmt.Println(string(data))
	//var Cur CityWeather
	//json.Unmarshal([]byte(data), &Cur)
	//fmt.Println(Cur.Main)
	//fmt.Println(Cur.Main.Temp - 273.15)
	//fmt.Println("The temparature is", Cur.Main.Temp-273.15, "degree in", Cur.Name, ". Humidity is", Cur.Main.Humidity, "percent. The general weather can be described as", Cur.Weather[0].Main, ".")
	//fmt.Println("The sunrise is at", time.Unix(int64(Cur.Sys.Sunrise), 0))
	//fmt.Println("The sunset is at", time.Unix(int64(Cur.Sys.Sunset), 0))
	//fmt.Println((5.0 / 9.0) * (Cur.Main.Temp - 32))
	//"The temperature is 33 degree in Dhaka. The chances of rain is 59%, humidity is 65%. You might need an umbrella when you go out. "

}
