package apiquery

import (
	"errors"
	"strconv"
	"strings"

	"gitlab.com/shajia/abhash-backend-only/data/api"
)

func isCity(name string) bool {
	_, ok := api.CityByID[name]

	if ok {
		return true
	}

	return false
}

func NewQuery(input string, q_type int) (string, error) {
	var q string
	var err error

	if q_type == 1 {
		q = type1(input)
	}

	if q_type == 2 {
		q = type2(input)
	}

	if q_type == 3 {
		q = type2(input)
	}

	if q_type == 4 {
		q = type1(input)
	}

	if q_type == 5 {
		q = type1(input)
	}

	if q == "" {
		err = errors.New("Unable to generate query string")
	}
	return q, err
}

func type1(input string) string {
	//https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=c7b8d20754001aa18e46f43d04b3a935
	words := strings.Split(input, " ")
	var id int
	id = -1

	for _, word := range words {
		if isCity(word) {
			id = api.CityByID[word]
			break
		}
	}

	if id == -1 {
		return ""
	}

	var ret string
	ret = "http://api.openweathermap.org/data//2.5/weather?id="
	ret = ret + strconv.Itoa(id)
	ret = ret + "&appid=c7b8d20754001aa18e46f43d04b3a935"

	return ret
}

func type2(input string) string {
	//https://samples.openweathermap.org/data/2.5/forecast/daily?id=524901&appid=b1b15e88fa797225412429c1c50c122a1

	words := strings.Split(input, " ")
	var id int
	id = -1

	for _, word := range words {
		if isCity(word) {
			id = api.CityByID[word]
			break
		}
	}

	if id == -1 {
		return ""
	}

	var ret string
	ret = "http://api.openweathermap.org/data/2.5/forecast?id="
	ret = ret + strconv.Itoa(id)
	ret = ret + "&appid=c7b8d20754001aa18e46f43d04b3a935"

	return ret
}

func type3(input string) string {
	return forecast(input)
}

func type4(input string) string {
	return forecast(input)
}

func type5(input string) string {
	return forecast(input)
}

func forecast(input string) string {
	//api.openweathermap.org/data/2.5/forecast?id=524901
	//http://api.openweathermap.org/data/2.5/forecast?q=Dhaka,bd&mode=xml&appid=c7b8d20754001aa18e46f43d04b3a935
	//http://api.openweathermap.org/data/2.5/forecast?id=1185241&mode=xml&appid=c7b8d20754001aa18e46f43d04b3a935

	words := strings.Split(input, " ")
	var id int
	id = -1

	for _, word := range words {
		if isCity(word) {
			id = api.CityByID[word]
			break
		}
	}

	if id == -1 {
		return ""
	}

	var ret string
	ret = "http://api.openweathermap.org/data/2.5/forecast?id="
	ret = ret + strconv.Itoa(id)
	ret = ret + "&mode=xml&appid=c7b8d20754001aa18e46f43d04b3a935"

	return ret
}
